package id.doceo.myapplication.model

import com.google.gson.annotations.SerializedName

data class RestOfficerModel (
    @SerializedName("error"      ) var error      : Boolean?          = null,
    @SerializedName("statuscode" ) var statuscode : Int?              = null,
    @SerializedName("values"     ) var values     : ArrayList<OfficerModel> = arrayListOf()
)

data class OfficerModel (
    @SerializedName("nik"        ) var nik       : Int?     = null,
    @SerializedName("first_name" ) var firstName : String?  = null,
    @SerializedName("last_name"  ) var lastName  : String?  = null,
    @SerializedName("alamat"     ) var alamat    : String?  = null,
    @SerializedName("aktif"      ) var aktif     : Boolean? = null
)