package com.wiz165.timebudget.helper

import android.util.Log
import kotlinx.coroutines.CoroutineExceptionHandler

object CoroutineHelper {
    private val TAG = "CoroutineException"

    fun getErrorHandler ():CoroutineExceptionHandler {
        val handler = CoroutineExceptionHandler({ coroutineContext, throwable ->
            Log.println(Log.ERROR, TAG, Log.getStackTraceString(throwable))
            throw throwable
        })

        return handler
    }
}