package id.doceo.myapplication.repository

import android.content.Context
import android.util.Log
import id.doceo.myapplication.model.OfficerModel
import id.doceo.myapplication.model.RestOfficerModel
import id.doceo.myapplication.network.ApiUtil
import id.doceo.myapplication.network.OfficerService
import timber.log.Timber

class OfficerRepo() {

    suspend fun getOfficer():RestOfficerModel? {
        val apiService = ApiUtil.retrofit.create(OfficerService::class.java)
        try {
            val res = apiService.getOfficer()
            return res
        } catch (e:Throwable) {
            Timber.e(e)
            return null;
        }
    }

    suspend fun addOfficer(data: OfficerModel):Boolean {
        val apiService = ApiUtil.retrofit.create(OfficerService::class.java)
        try {
            var list:ArrayList<OfficerModel> = arrayListOf()
            list.add(data)
            val res = apiService.insertOfficer(list)

            if (res.code() >= 200 && res.code() < 400) {
                return true
            } else {
                return false
            }
        } catch (e:Throwable) {
            Timber.e(e)
            return false
        }
    }
}