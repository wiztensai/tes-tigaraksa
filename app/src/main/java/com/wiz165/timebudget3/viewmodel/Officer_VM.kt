package id.doceo.myapplication.viewmodel

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import id.doceo.myapplication.model.OfficerModel
import id.doceo.myapplication.model.RestOfficerModel
import id.doceo.myapplication.repository.OfficerRepo
import kotlinx.coroutines.launch
import java.util.*

class Officer_VM(val bundle: Bundle?, val application: Application) : ViewModel() {

    private var repo:OfficerRepo
    val restOfficerModel = MutableLiveData<RestOfficerModel>()

    init {
        repo = OfficerRepo()

        viewModelScope.launch {
            getOfficer2()
        }
    }

    suspend fun getOfficer2() {
        viewModelScope.launch {
            val res = repo.getOfficer()
            restOfficerModel.value = res
        }
    }

    class VMFactory constructor(private val bundle: Bundle?, private val application: Application): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(Officer_VM::class.java)) {
                Officer_VM(bundle, application) as T
            } else {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }
    }
}