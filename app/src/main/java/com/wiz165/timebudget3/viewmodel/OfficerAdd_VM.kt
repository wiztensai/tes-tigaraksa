package id.doceo.myapplication.viewmodel

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import id.doceo.myapplication.model.OfficerModel
import id.doceo.myapplication.model.RestOfficerModel
import id.doceo.myapplication.repository.OfficerRepo
import kotlinx.coroutines.launch
import java.util.*

class OfficerAdd_VM(val bundle: Bundle?, val application: Application) : ViewModel() {

    val addResult = MutableLiveData<Boolean>()

    fun addOfficer(data:OfficerModel){
        var repo = OfficerRepo()
        viewModelScope.launch {
            var res = repo.addOfficer(data)

            if (res) {
                addResult.value = true
            } else {
                addResult.value = false
            }
        }
    }

    class VMFactory constructor(private val bundle: Bundle?, private val application: Application): ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(OfficerAdd_VM::class.java)) {
                OfficerAdd_VM(bundle, application) as T
            } else {
                throw IllegalArgumentException("ViewModel Not Found")
            }
        }
    }
}