package id.doceo.myapplication.ui

import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.wiz165.timebudget3.R
import com.wiz165.timebudget3.base.BaseActivity
import com.wiz165.timebudget3.databinding.ActivityOfficerAddBinding
import id.doceo.myapplication.model.OfficerModel
import id.doceo.myapplication.repository.OfficerRepo
import id.doceo.myapplication.viewmodel.OfficerAdd_VM
import id.doceo.myapplication.viewmodel.Officer_VM
import kotlinx.coroutines.launch

class OfficerAdd : BaseActivity() {

    lateinit var bind: ActivityOfficerAddBinding
    var active:Boolean = true
    lateinit var viewmodel: OfficerAdd_VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = ActivityOfficerAddBinding.inflate(layoutInflater)
        setContentView(bind.root)

        viewmodel = ViewModelProviders.of(this, OfficerAdd_VM.VMFactory(null, application)).get(OfficerAdd_VM::class.java)

        onClick()
        onStream()
    }

    private fun onStream() {
        viewmodel.addResult.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Input karyawan sukses", Toast.LENGTH_LONG).show()
                onBackPressed()
            } else {
                Toast.makeText(this, "Input karyawan gagal, mohon gunakan nik yang berbeda", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun onClick() {
        bind.btnAddOfficer.setOnClickListener {
            if (validate()) {
                Toast.makeText(applicationContext, "Mohon semua kolom diisi", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            var address = bind.etAddress.text.toString()
            var firstName = bind.etFirstName.text.toString()
            var lastName = bind.etLastName.text.toString()
            var nik = bind.etNIK.text.toString().toInt()

            var data = OfficerModel()
            data.aktif = active
            data.firstName = firstName
            data.lastName = lastName
            data.nik = nik
            data.alamat = address

            coroutineScope.launch {
                viewmodel.addOfficer(data)
            }
        }

        bind.rgActive.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbActive -> {
                    active = true
                }

                R.id.rbNonActive -> {
                    active = false
                }
            }
        }

        bind.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun validate() = bind.etAddress.text.isNullOrEmpty() || bind.etFirstName.text.isNullOrEmpty() || bind.etLastName.text.isNullOrEmpty() || bind.etNIK.text.isNullOrEmpty()
}