package id.doceo.myapplication.ui

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.wiz165.timebudget3.base.BaseActivity
import com.wiz165.timebudget3.databinding.ActivityOfficerBinding
import id.doceo.myapplication.model.OfficerModel
import id.doceo.myapplication.ui.recyclerview_epoxy.controller.EC_Officer
import id.doceo.myapplication.viewmodel.Officer_VM
import kotlinx.coroutines.launch

class Officer : BaseActivity() {

    lateinit var bind: ActivityOfficerBinding
    lateinit var epoxyController: EC_Officer
    lateinit var viewmodel: Officer_VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        bind = ActivityOfficerBinding.inflate(layoutInflater)
        setContentView(bind.root)

        viewmodel = ViewModelProviders.of(this, Officer_VM.VMFactory(null, application)).get(Officer_VM::class.java)

        initRv()
        onClick()
        onStream()
    }

    private fun initRv() {
        epoxyController = EC_Officer(applicationContext)
        epoxyController.isDebugLoggingEnabled = true

        bind.rvOfficer.layoutManager = LinearLayoutManager(baseContext)
        bind.rvOfficer.setController(epoxyController)
        bind.rvOfficer.setItemSpacingDp(8)
    }

    private fun onStream() {
        viewmodel.restOfficerModel.observe(this, Observer {
            epoxyController.officerModels.addAll(it.values)
            bind.rvOfficer.requestModelBuild()
        })
    }

    private fun onClick() {
        bind.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }
}