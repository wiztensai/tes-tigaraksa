package id.doceo.myapplication.ui.recyclerview_epoxy.model

import android.content.Context
import android.content.res.Resources
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.airbnb.epoxy.kotlinsample.helpers.KotlinEpoxyHolder
import com.wiz165.timebudget3.R
import id.doceo.myapplication.model.OfficerModel
import java.util.*

@EpoxyModelClass(layout = R.layout.item_officer)
abstract class EM_Officer() : EpoxyModelWithHolder<EM_Officer.Holder>() {

    @EpoxyAttribute
    lateinit var model: OfficerModel

    override fun bind(holder: Holder) {
        super.bind(holder)
        holder.tvNIK.setText(model.nik.toString())
        holder.tvName.setText("${model.firstName} ${model.lastName}")
        holder.tvAddress.setText(model.alamat)
    }

    inner class Holder : KotlinEpoxyHolder() {
        val tvNIK by bind<TextView>(R.id.tvNIK)
        val tvAddress by bind<TextView>(R.id.tvAddress)
        val tvName by bind<TextView>(R.id.tvName)
    }
}
