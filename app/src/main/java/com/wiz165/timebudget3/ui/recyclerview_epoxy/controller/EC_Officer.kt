package id.doceo.myapplication.ui.recyclerview_epoxy.controller

import android.content.Context
import com.airbnb.epoxy.EpoxyController
import id.doceo.myapplication.model.OfficerModel
import id.doceo.myapplication.ui.recyclerview_epoxy.model.EM_Officer_
import timber.log.Timber
import java.lang.RuntimeException

class EC_Officer(val context: Context) : EpoxyController() {

    var officerModels = mutableListOf<OfficerModel>()

    override fun buildModels() {
        if (officerModels.isEmpty()) {
        } else {
            officerModels.forEach {
                EM_Officer_()
                    .id("officer")
                    .model(it)
                    .addTo(this)
            }
        }

    }

    override fun onExceptionSwallowed(exception: RuntimeException) {
        super.onExceptionSwallowed(exception)
        Timber.e(exception)
    }
}