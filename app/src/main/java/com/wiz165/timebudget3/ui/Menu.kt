package id.doceo.myapplication.ui

import android.content.Intent
import android.os.Bundle
import com.wiz165.timebudget3.base.BaseActivity
import com.wiz165.timebudget3.databinding.ActivityMenuBinding

class Menu : BaseActivity() {

    lateinit var bind:ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(bind.root)

        onClick()
    }

    private fun onClick() {
        bind.btnAddOfficer.setOnClickListener {
            startActivity(Intent(this, OfficerAdd::class.java))
        }

        bind.btnListOfficer.setOnClickListener {
            startActivity(Intent(this, Officer::class.java))
        }
    }
}