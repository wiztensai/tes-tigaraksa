package com.wiz165.timebudget3.base
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.wiz165.timebudget.helper.CoroutineHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob


open class BaseActivity: AppCompatActivity() {

    val coroutineScope = CoroutineScope(
        SupervisorJob() + CoroutineHelper.getErrorHandler()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}