package id.doceo.myapplication.network

import id.doceo.myapplication.model.OfficerModel
import id.doceo.myapplication.model.RestOfficerModel
import retrofit2.Response
import retrofit2.http.*


interface OfficerService {
    @POST("karyawan/insert")
    suspend fun insertOfficer2(
        @Field("nik") nik : Int,
        @Field("first_name") first_name : String,
        @Field("last_name") last_name : String,
        @Field("alamat") alamat : String,
        @Field("aktif") aktif : Boolean
    )

    @POST("karyawan/insert")
    suspend fun insertOfficer(@Body model: ArrayList<OfficerModel>): Response<Unit>

    @GET("api/karyawan/all")
    suspend fun getOfficer(): RestOfficerModel
}
